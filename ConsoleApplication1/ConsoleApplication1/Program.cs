﻿using ConsoleApplication1.Concrete;
using ConsoleApplication1.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Привет! Эта программа выводит на экран список людей в формате JSON.");

            IRepository repository = new FakeRepository();
            var people = repository.GetPeople();
            string json = JsonConvert.SerializeObject(people, Formatting.Indented);
            Console.WriteLine(json);


            Console.WriteLine("конец");
            Console.ReadKey();
        }
    }
}
