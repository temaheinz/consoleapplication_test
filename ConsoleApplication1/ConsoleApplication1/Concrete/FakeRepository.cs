﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApplication1.Entities;

namespace ConsoleApplication1.Concrete
{
    class FakeRepository : IRepository
    {
        public IEnumerable<Person> GetPeople()
        {
            yield return new Person { Id = 1, FirstName = "Иван", LastName = "Иванов" };
            yield return new Person { Id = 2, FirstName = "Петр", LastName = "Петров" };
            yield return new Person { Id = 3, FirstName = "Александр", LastName = "Александров" };
            yield return new Person { Id = 3, FirstName = "Алексей", LastName = "Алексеев" };
        }
    }
}
